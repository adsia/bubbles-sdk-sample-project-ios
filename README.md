# BubblesSDK #

Bubbles iOS SDK gives you the ability to provide your customers with great In-App customer care services. To have trial access, kindly submit your access request through [Bubbles.cc](http://bubbles.cc/) website.

This repo shows an example of how you can integrate the service with your great iOS mobile applications.


## Installation ##
-----------

1. Clone this example project
2. From the BubblesSDK folder, copy BubblesSDK.framework and BubblesSDK.bundle to your project.
3. Link your project against the following frameworks:
    - libicucore.dylib
    - Security.framework
    - CFNetwork.framework
    - WebKit.framework
    - SystemConfiguration
4. Now you are good to go, just import <BubblesSDK/BubblesSDK.h> in any file that needs to access the SDK files.


## Getting Started ##
-----------

First, you will need an API key. 

* Once you got your server access. Generate your SDK key from the "Settings" tab in your Bubbles server. 
* Note down your API key because you will need it in your code.
* Install the SDK as mentioned above.
* To start using the SDK, first you need to tell it your server name and your API Key. You do this by calling the following method:

```
    [ConfigManager setServerName:@"YOUR_SERVER_NAME" APIKey:@"YOUR_SERVER_API_KEY"];
```



### Starting the chat ###
-----------

To start chat from your mobile application:

1. Set the server name and API key as mentioned above.
2. Make your view controller (or whatever Objective-C class, depends on your application architecture) conform to both protocols: ChatSessionDelegate and ChatMessagesDelegate.
3. Perform the following method calls:
```
    //self refers to whatever object who conforms to those protocols
    [[ChatManager instance] setSessionDelegate:self];
    [[ChatManager instance] setChatMessagesDelegate:self];
    //Only the name parameter must not be nil.
    [[ChatManager instance] enterChatWithCustomerName:name customerId:userId customerLocation:userLocation deviceToken:deviceToken];
```

And that's it!

### API References ###
-----------
For detailed information about each class, protocol, method and type check out the docs folder.

### Video Tutorial ###
-----------
 [How to add live chat to your iOS mobile app with Bubbles SDK](https://www.youtube.com/watch?v=RCVNek2Dpcg)

## Apps using Bubbles SDK ##
-----------

* [Bubbles Care](https://itunes.apple.com/us/app/bubbles.cc/id922468338)
* [Infinity Advertising Care](https://itunes.apple.com/us/app/infinity-advertising-care/id957279659)
* [Ketwket](https://itunes.apple.com/us/app/kyt-wkyt-ketwket/id819371940)