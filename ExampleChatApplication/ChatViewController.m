//
//  ChatViewController.m
//  ExampleChatApplication
//
//  Created by Ahmed Ali on 1/22/15.
//  Copyright (c) 2015 Ahmed Ali. All rights reserved.
//

#import "ChatViewController.h"
#import "ChatTableViewCell.h"
#import <BubblesSDK/BubblesSDK.h>

#import "TSMessage.h"
#import "MRProgress.h"

@interface ChatViewController ()<ChatMessagesDelegate, ChatSessionDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (nonatomic) NSArray * departments;
@property (nonatomic) DepartmentModel * selectedDepartment;

@property (weak, nonatomic) IBOutlet UIPickerView * departmentsPickerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *departmentsBottomSpace;

@property (weak, nonatomic) IBOutlet UIView *waitingContainer;
@property (weak, nonatomic) IBOutlet UILabel *numberOfFrontCustomer;

@property (weak, nonatomic) IBOutlet UILabel *expectedWaitingTime;
//constant = 0 when keyboard is hidden
//constant = keyboardHight when it is shown
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chatInputContainerBottomSpaceContainer;


//0 hidden, -30 visible
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *isTypingTopConstraint;



@property (weak, nonatomic) IBOutlet UITextField *inputTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@property (weak, nonatomic) IBOutlet UITableView *messagesTableView;


@property (nonatomic) NSMutableArray * messages;


@property (nonatomic) ChatTableViewCell * measuringCell;

@end

@implementation ChatViewController

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.messages = [NSMutableArray array];
    [self beginChat];
    [self watchKeyboard];
    self.messagesTableView.rowHeight = UITableViewAutomaticDimension;
    self.messagesTableView.estimatedRowHeight = 44.0;
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self stopWatchingKeyboard];
}


#pragma mark - handle keyboard show and hide
/**
 Registers observers for keyboard show/hide events
 */
-(void)watchKeyboard
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

/**
 Unregisters keyboard show/hide notification observers
 */
-(void)stopWatchingKeyboard
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

/**
 Resizes the chat messages table with animation synced with the keyboard appearance animation
 
 */
-(void)keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    
    
    self.chatInputContainerBottomSpaceContainer.constant = keyboardBounds.size.height;
    
    //	// animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    [self.view layoutIfNeeded];
    [UIView commitAnimations];
    [self scrollToLatestMessage];
}



/**
 Resizes the chat messages table with animation synced with the keyboard disappearance animation
 
 */
-(void) keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    self.chatInputContainerBottomSpaceContainer.constant = 0;
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    [self.view layoutIfNeeded];
    // commit animations
    [UIView commitAnimations];
    
    
}

#pragma mark - Helper methods


-(void)beginChat
{
    [self disableSendingMessages];
    [MRProgressOverlayView showOverlayAddedTo:self.view animated:YES];
    ChatManager * chatManager = [ChatManager instance];
    chatManager.sessionDelegate = self;
    chatManager.chatMessagesDelegate = self;
    [chatManager enterChatWithCustomerName:self.customerName customerId:nil customerLocation:nil deviceToken:nil];
}


- (IBAction)endChat:(id)sender
{
    [[ChatManager instance] customerLeftChat];
    [self performSegueWithIdentifier:@"feedback" sender:self];
}


-(void)enabelSendingMessages
{
    self.waitingContainer.hidden = YES;
    self.inputTextField.enabled = YES;
}

-(void)disableSendingMessages
{
    self.inputTextField.enabled = NO;
}

-(void)showDepartmentsSelection
{
    self.departmentsBottomSpace.constant = 0;
    [self.departmentsPickerView reloadAllComponents];
    self.selectedDepartment = self.departments.firstObject;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(void)showIsTyping
{
    self.isTypingTopConstraint.constant = -30;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(void)hideIsTyping
{
    self.isTypingTopConstraint.constant = 0;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self scrollToLatestMessage];
    }];
    
}

- (IBAction)sendMessage:(id)sender
{
    NSString * message = self.inputTextField.text;
    if(message == nil || message.length < 1){
        return;
    }
    self.inputTextField.text = @"";
    ChatMessageModel * chatMessage = [[ChatMessageModel alloc] init];
    chatMessage.msg = message;
    
    [[ChatManager instance] customerStoppedTyping];
    chatMessage.nick = self.customerName;
    chatMessage.avatar = @"http://png-1.findicons.com/files/icons/1072/face_avatars/300/i04.png";
    chatMessage.who = CustomerMessage;
    [[ChatManager instance] sendChatMessage:chatMessage];
    [self.messages addObject:chatMessage];
    
    [self showLatestMessage];
}

/**
 Adds a cell for the latest message
 */
-(void)showLatestMessage
{
    if(self.messages.count == 0){
        return;
    }
    if([self.messagesTableView numberOfRowsInSection:0] < self.messages.count){
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:self.messages.count - 1 inSection:0];
        [self.messagesTableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self scrollToLatestMessage];
    }else{
//        [self.messagesTableView reloadData];
        [self scrollToLatestMessage];
    }
    
    
}

/**
 Scrolls to the latest message in the chat messages table
 */

-(void)scrollToLatestMessage
{
    if(self.messages.count == 0){
        //nothing to do
        return;
    }
    
    
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:self.messages.count - 1 inSection:0];
    double delayInSeconds = 0.2;
    __weak typeof(self) weakSelf = self;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [weakSelf.messagesTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    });
}

- (IBAction)selectDepartment:(id)sender
{
    self.departmentsBottomSpace.constant = 230;
    [ChatManager instance].selectedDepartmentId = self.selectedDepartment.departmentId;
    [self beginChat];
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"]) {
        
        [textField resignFirstResponder];
        [[ChatManager instance] customerStoppedTyping];
        
        return NO;
    }
    [[ChatManager instance] customerStartTyping];
    return YES;
}



#pragma mark - UIPickerViewDataSource
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.departments.count;
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)componen
{
    DepartmentModel * department = self.departments[row];
    return department.departmentName;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.selectedDepartment = self.departments[row];
}


#pragma mark - UITabelViewDelegate
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if(self.measuringCell == nil){
//        self.measuringCell = [tableView dequeueReusableCellWithIdentifier:@"customerCell"];
//    }
//    
//    ChatMessageModel * message = self.messages[indexPath.row];
//    self.measuringCell.message = message;
//    return [self.measuringCell neededHeight];
//}



#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.messages.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * identifier = @"customerCell";
    ChatMessageModel * message = self.messages[indexPath.row];
    if([message.who isEqualToString:OperatorMessage]){
        identifier = @"operatorCell";
    }
    ChatTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell.message = self.messages[indexPath.row];
    
    return cell;
}

#pragma mark - BubblesSDK
#pragma mark - ChatSessionDelegate
/**
 Called when a previously broken session connection has been re-established and the session re-joined successfully
 
 @param chatManager ChatManager instance
 */
-(void)chatManagerSessionDidRestore:(ChatManager *)chatManager
{
    
    if(chatManager.serverBusy){
        [self chatManager:chatManager waitingQueueDidUpdate:chatManager.waitingData];
    }else{
        [TSMessage showNotificationWithTitle:@"Connected" subtitle:@"Chat is resumed" type:TSMessageNotificationTypeSuccess];
        self.waitingContainer.hidden = YES;
        [self enabelSendingMessages];
    }
    
}


/**
 Called when an attempt to start a new chat session is failed
 
 @param chatManager ChatManager instance
 @param errorMessage human-friendly message describes the potential cause of the error
 */
-(void)chatManager:(ChatManager *)chatManager didFailToEnterChat:(NSString *)errorMessage
{
    [self disableSendingMessages];
    [TSMessage showNotificationWithTitle:errorMessage type:TSMessageNotificationTypeError];
    [MRProgressOverlayView dismissAllOverlaysForView:self.view animated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
}




/**
 Called when a session is successfully started and a welcome message is received
 
 @param chatManager ChatManager instance
 @param welcomeMessage the human-friendly welcome message set for your server
 */
-(void)chatManager:(ChatManager *)chatManager didEnterChat:(NSString *)welcomeMessage
{
    [MRProgressOverlayView dismissAllOverlaysForView:self.view animated:YES];
    [TSMessage showNotificationWithTitle:welcomeMessage type:TSMessageNotificationTypeMessage];
    if(chatManager.serverBusy){
        [self chatManager:chatManager waitingQueueDidUpdate:chatManager.waitingData];
    }else{
        [self enabelSendingMessages];
    }
    
}


/**
 Called when there is an update in the waiting queue
 
 @param chatManager ChatManager instance
 @param waitingData describes the current waiting status
 */
-(void)chatManager:(ChatManager *)chatManager waitingQueueDidUpdate:(ChatWaitingModel *)waitingData
{
    self.numberOfFrontCustomer.text = [NSString stringWithFormat:@"There are %li customer(s) in front of you.", (long)waitingData.customersInFront];
    self.expectedWaitingTime.text = [NSString stringWithFormat:@"Your chat is expected to start within the next %li minute(s)", (long)waitingData.expectedWaitingTime];
    [self disableSendingMessages];
    self.waitingContainer.hidden = NO;
}

/**
 If the operator transfered the customer to another department and the customer entered a queue in the other department this method will be called
 
 @param chatManager ChatManager instance
 @param waitingData describes the current waiting status
 */
-(void)chatManager:(ChatManager *)chatManager didEnterWaitingQueueAfterTransfer:(ChatWaitingModel *)waitingData
{
    [self chatManager:chatManager waitingQueueDidUpdate:waitingData];
}

/**
 Called when the operator transfers the customer to another department
 
 @param chatManager ChatManager instance
 */
-(void)chatManagerSessionIsBeingTransfered:(ChatManager *)chatManager
{
    [TSMessage showNotificationWithTitle:@"You are being transfered" type:TSMessageNotificationTypeMessage];
    self.waitingContainer.hidden = YES;
    [self disableSendingMessages];
}



/**
 Called when the current established connection is broken for any reason
 
 @param chatManager ChatManager instance
 @param error check the "message" key in error.userInfo dictionary
 */
-(void)chatManager:(ChatManager *)chatManager connectionDidFail:(NSError *)error
{
    [self disableSendingMessages];
    [TSMessage showNotificationWithTitle:error.userInfo[@"message"] type:TSMessageNotificationTypeError];
}

/**
 Called when the operator is disconnected
 
 @param chatManager ChatManager instance
 */
-(void)chatManagerOperatorDidDisconnect:(ChatManager *)chatManager
{
    [self disableSendingMessages];
    [TSMessage showNotificationWithTitle:@"Chat is disconnected, attempting to reconnect" type:TSMessageNotificationTypeError];
}

/**
 Called when the operator is connected after he had a broken connection
 
 @param chatManager ChatManager instance
 */
-(void)chatManagerOperatorDidReconnect:(ChatManager *)chatManager
{
    if(chatManager.serverBusy){
        //we are still in the waiting queue
        [self disableSendingMessages];
        [self chatManager:chatManager waitingQueueDidUpdate:chatManager.waitingData];
        
    }else{
        [self enabelSendingMessages];
        [TSMessage showNotificationWithTitle:@"Chat is resumed" type:TSMessageNotificationTypeSuccess];
    }
    
}

/**
 Called when you enter a chat, and there are more than one department to select from
 When called, set the selectedDepartmentId value and then call enterChatWithCustomerName: customerId: customerLocation: deviceToken: method again
 
 @param chatManager ChatManager instance
 @param departments dictionary, where the key is department name and the value is the department id
 */
-(void)chatManager:(ChatManager *)chatManager aDepertmentMustBeSelectedFirst:(NSArray *)departments
{
    //dismss the loader
    [MRProgressOverlayView dismissAllOverlaysForView:self.view animated:YES];
    //show depeartments selection
    self.departments = departments;
    [self showDepartmentsSelection];
    
}

/**
 Called when an operator opens the chat session that the customer entered
 
 After the customer enters a chat, he should not send any message til this method is called.
 
 @param chatManager ChatManager instance
 @param message that can be displayed to the user to let him know that the operator is joined
 */
-(void)operatorDidJoin:(ChatManager *)chatManager joinMessage:(NSString *)message
{
    [self enabelSendingMessages];
    [MRProgressOverlayView dismissAllOverlaysForView:self.view animated:YES];
    
    [TSMessage showNotificationWithTitle:message type:TSMessageNotificationTypeMessage];
}


#pragma mark - ChatMessagesDelegate
/**
 Called when there are messages those sent by the user and failed to be delivered to the server
 
 @param chatManager ChatManager instance
 @param failedMessages array of ChatMessageModel that failed to be delivered
 */
-(void)chatManager:(ChatManager *)chatManager didFailToDeliverMessages:(NSArray *)failedMessages
{
    NSLog(@"Failed to deliver messages: %@", failedMessages);
    NSMutableArray * indexPaths = [NSMutableArray array];
    for(ChatMessageModel * message in failedMessages){
        NSInteger index = [self.messages indexOfObject:message];
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        [indexPaths addObject:indexPath];
    }
    
    [self.messagesTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
}


/**
 Called when the message is successfully sent to the server
 
 @param chatManager ChatManager instance
 @param message the successfully sent message
 */
-(void)chatManager:(ChatManager *)chatManager didSuccessToDeliverMessage:(ChatMessageModel *)message
{
    NSInteger index = [self.messages indexOfObject:message];
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    ChatTableViewCell * cell = (ChatTableViewCell *)[self.messagesTableView cellForRowAtIndexPath:indexPath];
    [cell refreshUI];
}


/**
 Called when a new message is received
 
 @param chatManager ChatManager instance
 @param message     ChatMessageModel the newly recieved message
 */
-(void)chatManager:(ChatManager *)chatManager didReceiveMessage:(ChatMessageModel *)message
{
    [self.messages addObject:message];
    [self showLatestMessage];
}


/**
 Called when the operator is typing on his keyboard
 
 @param chatManager ChatManager instance
 */
-(void)operatorIsTyping:(ChatManager *)chatManager
{
    [self showIsTyping];
}


/**
 Called when the operator is stopped typing
 
 @param chatManager ChatManager instance
 */
-(void)operatorStoppedTyping:(ChatManager *)chatManager
{
    [self hideIsTyping];
}


/**
 Called when the operator leave the chat session. After the call of this method, no messages will be delivered.
 
 @param chatManager ChatManager instance
 @param chatEndMessage the bye bye message set in your server
 */
-(void)operatorDidLeftChat:(ChatManager *)chatManager chatEndMessaeg:(NSString *)chatEndMessage
{
    [self disableSendingMessages];
    [TSMessage showNotificationWithTitle:chatEndMessage type:TSMessageNotificationTypeMessage];
}


/**
 Called when the customer stop typing or sending messages for the a while.
 If the maximum allowed ideal time is 6 minutes, this method will be called if the user stop typing or sending messages for more than 3 minutes
 
 @param chatManager ChatManager instance
 @param idealNotificationMessage the ideal notification message set in the server configuration
 */
-(void)chatManagerUserIsIdeal:(ChatManager *)chatManager idealNotificationMessage:(NSString *)idealNotificationMessage
{
    [TSMessage showNotificationWithTitle:idealNotificationMessage type:TSMessageNotificationTypeWarning];
}


/**
 Called when the customer stop typing or sending messages for the a while.
 If the maximum allowed ideal time is 6 minutes, this method will be called if the user stop typing or sending messages for more than 6 minutes
 
 @param chatManager ChatManager instance
 @param chatEndMessage the chat end message set in the server
 */
-(void)chatManagerUserDidTimeOut:(ChatManager *)chatManager chatEndMessag:(NSString *)chatEndMessage
{
    [self disableSendingMessages];
    [TSMessage showNotificationWithTitle:chatEndMessage type:TSMessageNotificationTypeError];
}



@end
