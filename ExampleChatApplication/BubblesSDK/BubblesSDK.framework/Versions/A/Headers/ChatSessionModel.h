//
//  ChatLog.h
//  Bubbles
//
//  Created by Ahmed Ali on 1/3/14.
//  Copyright (c) 2014 Ahmed Ali. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ChatMessageModel.h"

/**
 Represents a chat session/conversation used when mapping json server response
 */
@interface ChatSessionModel : NSObject
/**
 Session log id
 */
@property (nonatomic, strong) NSString * logId;

/**
 When were it started as a micro-seconds
 */
@property (nonatomic) double startTime;

/**
 What was the user rate
 */
@property (nonatomic) NSInteger userRate;

/**
 The feedback comment
 */
@property (nonatomic, strong) NSString * userComment;

/**
 List of the session log messages
 */
@property (nonatomic, strong) NSArray * log;


-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;

@end
