//
//  FaqTopicModel.h
//  bubbles
//
//  Created by Ahmed Ali on 7/22/14.
//  Copyright (c) 2014 Ahmed Ali. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FaqTopicModel @end
@interface FaqTopicModel : NSObject

/**
 Question
 */
@property (nonatomic) NSString * question;

/**
 Answer
 */
@property (nonatomic) NSString * answer;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end
