//
//  AboutModel.h
//  bubbles
//
//  Created by Ahmed Ali on 7/20/14.
//  Copyright (c) 2014 Ahmed Ali. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AboutModel : NSObject

/**
 Address
 */
@property (nonatomic, strong) NSString * address;

/**
 Details
 */
@property (nonatomic, strong) NSString * details;

/**
 Email
 */
@property (nonatomic, strong) NSString * email;

/**
 Facebook
 */
@property (nonatomic, strong) NSString * facebook;

/**
 Google+
 */
@property (nonatomic, strong) NSString * googleplus;

/**
 Image
 */
@property (nonatomic, strong) NSString * image;

/**
 Instagram
 */
@property (nonatomic, strong) NSString * instagram;

/**
 Phone
 */
@property (nonatomic, strong) NSString * phone;

/**
 Twitter
 */
@property (nonatomic, strong) NSString * twitter;

/**
 Website
 */
@property (nonatomic, strong) NSString * website;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;

@end
