//
//  UserLocation.h
//  ketwket
//
//  Created by Ahmed on 3/4/14.
//  Copyright (c) 2014 Ahmed Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
/**
 Represents a user location details
 */
@interface CustomerLocation : NSObject

/**
 Logitude
 */
@property (nonatomic) CLLocationDegrees longitude;
/**
 Latitude
 */
@property (nonatomic) CLLocationDegrees latitude;
/**
 Country
 */
@property (nonatomic) NSString * country;
/**
 County
 */
@property (nonatomic) NSString * county;

/**
 City
 */
@property (nonatomic) NSString * city;

/**
 Street
 */
@property (nonatomic) NSString * street;


/**
 Converts all the data to dictionary
 */
-(NSDictionary *)toDictionary;

@end
