//
//  FaqManager.h
//  bubbles
//
//  Created by Ahmed Ali on 7/22/14.
//  Copyright (c) 2014 Ahmed Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResultModel.h"

@interface FaqManager : NSObject

/**
 Holds array of all FAQ topics
 */
@property (nonatomic) NSArray * allQuestions;

/**
 Holds array of all FAQ categories
 */
@property (nonatomic) NSArray * categories;

+(instancetype)instance;

/**
 Loads the FAQ data
 
 After a success call to this method the categories property should have a value If SessionConfigManager.config.faqCategories returns YES, else if If SessionConfigManager.config.faqCategories returns NO the allQuestions property will have a value.
 This method makes an HTTP request, so you should avoid calling it from the UI thread.
 
 @return result to determine if the request was success or no, and the reason in case of failure
 */
-(ResultModel *)loadFaqs;

/**
 Whether there is a cache for the FAQ data.
 */
-(BOOL)needsToLoadFaqsData;

@end
