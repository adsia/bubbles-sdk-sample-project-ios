//
//  Constants.h
//  iOSBubblesSDK
//
//  Created by Ahmed Ali on 11/6/14.
//  Copyright (c) 2014 Ahmed Ali. All rights reserved.
//

#import <UIKit/UIKit.h>

extern const NSString * kErrorMessageKey;
extern const NSUInteger ERROR_PARSING_JSON;
