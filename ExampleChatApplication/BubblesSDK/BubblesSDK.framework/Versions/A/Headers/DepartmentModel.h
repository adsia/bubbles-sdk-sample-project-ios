//
//  DepartmentModel.h
//  BubblesSDK
//
//  Created by Ahmed Ali on 1/22/15.
//  Copyright (c) 2015 Ahmed Ali. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DepartmentModel : NSObject
@property (nonatomic) NSString * departmentName;
@property (nonatomic) NSString * departmentId;
@end
