//
//  ConfigManager.h
//  BubblesSDK
//
//  Created by Ahmed Ali on 12/18/14.
//  Copyright (c) 2014 Ahmed Ali. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConfigManager : NSObject

/**
 Call this method to set your server name and API key. Make sure to call this method before dealing with any other API to make sure everything works as expected.
 */
+(void)setServerName:(NSString *)serverName APIKey:(NSString *)apiKey;
@end
