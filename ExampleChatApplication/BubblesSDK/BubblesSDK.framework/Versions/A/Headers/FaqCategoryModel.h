//
//  FaqCategoryModel.h
//  bubbles
//
//  Created by Ahmed Ali on 7/22/14.
//  Copyright (c) 2014 Ahmed Ali. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FaqTopicModel.h"
@interface FaqCategoryModel : NSObject
/**
 Category title
 */
@property (nonatomic) NSString * title;

/**
 Number of topics in the category
 */
@property (nonatomic) NSInteger topicsCount;

/**
 Topics of the category
 */
@property (nonatomic) NSArray * topics;


-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end
