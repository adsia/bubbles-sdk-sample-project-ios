//
//  AboutManager.h
//  bubbles
//
//  Created by Ahmed Ali on 7/20/14.
//  Copyright (c) 2014 Ahmed Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResultModel.h"
#import "AboutModel.h"
@interface AboutManager : NSObject

@property (nonatomic) AboutModel * aboutData;

+(instancetype)instance;

/**
 If the aboutData property is nil, call this method to load and cache the about data.
 
 This method makes an HTTP request, so you should avoid calling it from the UI thread.

 @return result to determine if the request was success or no, and the reason in case of failure.
 */
-(ResultModel *)loadAboutData;

@end
