//
//  ChatHistoryManager.h
//  BubblesSDK
//
//  Created by Ahmed Ali on 12/17/14.
//  Copyright (c) 2014 Ahmed Ali. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatHistoryManager : NSObject


+(instancetype)instance;


/**
 Loads and returns array of latest chat sessions for the passed userId.
 
 @param userId the user id to fetch its chat sessions
 @param timestamp the timestamp to return any chat sessions made after it.
 @param error a pointer to pointer to fill with error details in case of failure. Lookup the error.userInfo[@"message"] value for the reason of failure if any.
 
 This method makes an HTTP request, so you should avoid calling it from the UI thread.
 @return array of chat sessions
 */
-(NSArray *)loadLatestChatSessionsForUserId:(NSString *)userId mostRecentSessionTimestamp:(double)timestamp error:(NSError * __autoreleasing *)error;

/**
 Loads and returns array of chat sessions for the passed userId.
 
 @param userId the user id to fetch its chat sessions
 @param sessionsCountPerPage the number of sessions you want to fetch and display in each request.
 @param pageNumber the number of the current page you are requesting. Starting from 1.
 @param numberOfCachedSessions the number of previously cached sessions
 @param error a pointer to pointer to fill with error details in case of failure. Lookup the error.userInfo[@"message"] value for the reason of failure if any.
 
 This method makes an HTTP request, so you should avoid calling it from the UI thread.
 @return array of chat sessions
 */
-(NSArray *)chatSessionsForUserId:(NSString *)userId sessionsCountPerPage:(NSUInteger)sessionsCountPerPage pageNumber:(NSUInteger)pageNumber numberOfCachedSessions:(NSUInteger)numberOfCachedSessions error:(NSError * __autoreleasing *)error;

/**
 Loads the messages of the passed sessionId
 
 @param sessionId the session id to use while fetching the messages
 @param error a pointer to pointer to fill with error details in case of failure. Lookup the error.userInfo[@"message"] value for the reason of failure if any.
 
 @return array of chat messages
 */
-(NSArray *)loadChatMessagesForSessionId:(NSString *)sessionId error:(NSError * __autoreleasing *)error;
@end
