//
//  ResultModel.h
//  BubblesSDK
//
//  Created by Ahmed Ali on 12/17/14.
//  Copyright (c) 2014 Ahmed Ali. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Describes the result of any method that performs HTTP requests
 */
@interface ResultModel : NSObject

/** Whether the operation is success */
@property (nonatomic) BOOL success;

/** The error message if the operation is failed. */
@property (nonatomic) NSString * errorMessage;
@end
