//
//  ChatMessage.h
//  Bubbles
//
//  Created by Ahmed Ali on 12/12/13.
//  Copyright (c) 2013 Ahmed Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

/**
 The current message status
 */
typedef NS_ENUM(NSUInteger, MessageStatus) {
    /** Message fialed to be sent to the server */
    MESSAGE_FAILED,
    
    /** Message sent and delivered successfully to the backend (not the operator, only the server) */
    MESSAGE_DELIVERED,
    
    /** Message sent to the server, not delivered yet */
    MESSAGE_SENT,
    
    /** Message received from the server */
    MESSAGE_RECEIVED
};


/**
 Represents a chat message, used when dealing with the web service
 */
@interface ChatMessageModel : NSObject

/**
 Name of the message sender
 */
@property (nonatomic, strong) NSString * nick;

/**
 Message body
 */
@property (nonatomic, strong) NSString * msg;


/**
 The full Url to the avatar image
 */
@property (nonatomic, strong) NSString * avatar;

/**
 Defines the sender type, op or cu, which means operator or customer respectively
 */
@property (nonatomic, strong) NSString * who;


/**
 Timestamp when the message was sent
 */
@property (nonatomic) NSTimeInterval timestamp;

/**
 Use this value to determine the status of the message
 */
@property (nonatomic) MessageStatus status;

/**
 The chat room ID, normally you will not use it.
 */
@property (nonatomic, strong) NSString * room;

/**
 Server name
 */
@property (nonatomic, strong) NSString * prefix;

/**
 Creates an instance using the passed json string.
 
 @param json the json string to try to parse and fetch its data
 @param error a pointer to pointer which will contain a descriptive error message in case of error
 */
-(instancetype)initWithJsonString:(NSString *)json error:(NSError * __autoreleasing *)error;

/**
 Creates an instance using the passed json string.
 
 @param dictionary the dictionar to fetch its data
 @param error a pointer to pointer which will contain a descriptive error message in case of error
 */
-(instancetype)initWithDictionary:(NSDictionary *)dictionary error:(NSError * __autoreleasing *)error;

/**
 Converts the current instance to dictionary where the key is the property name and the value is the property value

 @return the converted dictionary
 */
-(NSDictionary *)toDictionary;
@end
