//
//  ChatCacheManagerDelegate.h
//  ketwket
//
//  Created by Ahmed Ali on 6/18/14.
//  Copyright (c) 2014 Ahmed Ali. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Defines the needed methods to provide a cache manager for chat sessions
 */
@protocol ChatCacheManagerDelegate <NSObject>

/** 
 Called when a new session starts
 
 Use this method to create a new record for the newly started chat session
 @param sessionId unique id for the session
 @param startTime the session start timestamp
 @param userId used to relate the sessions with the user id
 */
-(void)createNewSession:(NSString *)sessionId startTime:(double)startTime userId:(NSString *)userId;

/**
 Called to set the session rate data
 
 Use this method to update feedback comment and rate for the session with the passed sessionId
 @param rate the feedback rate
 @param comment the feedback comment
 @param sessionId the session id
 */
-(void)updateSessionRate:(float)rate comment:(NSString *)comment sessionId:(NSString *)sessionId;

/**
 Notifies the delegate to add the passed message to the session's messages queue
 
 @param chatMessage the message to add
 @param sessionId the sessionId
 */
-(void)cacheChatMessage:(ChatMessageModel *)chatMessage sessionId:(NSString *)sessionId;

/**
 Called to set the end time for the chat session
 
 @param endTime the chat end timestamp
 @param sessionId the chat session id
 */
-(void)updateSessionEndTime:(double)endTime sessionId:(NSString *)sessionId;

/**
 Called when the current chat session ends for any final clean up
 */
-(void)cleanUp;
@end
