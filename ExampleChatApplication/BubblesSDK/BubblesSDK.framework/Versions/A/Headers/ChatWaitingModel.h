//
//  ChatWaitingModel.h
//  ketwket
//
//  Created by Ahmed on 3/30/14.
//  Copyright (c) 2014 Ahmed Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 Representing the chat queue status and data
 */
@interface ChatWaitingModel : NSObject

/** Expected waiting time in mininutes */
@property (nonatomic) NSInteger expectedWaitingTime;

/** How many customers in front of the current customer in the queue */
@property (nonatomic) NSInteger customersInFront;


/** The human-friendly message to show */
@property (nonatomic, readonly) NSString * waitingMessage;

/** Creates intance with the provided paramas
 
 @param customersInFront the number of customers in front
 @param expectedWaitingTime the expected waiting time
 */
-(instancetype)initWithCustomersInFront:(NSInteger)customersInFront expectedWaitingTime:(NSInteger)expectedWaitingTime;
@end
