//
//  ChatManager.h
//  Bubbles
//
//  Created by Ahmed Ali on 12/12/13.
//  Copyright (c) 2013 Ahmed Ali. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "ChatMessageModel.h"
#import "CustomerLocation.h"
#import "ChatWaitingModel.h"
#import "ChatCacheManagerDelegate.h"
#import "DepartmentModel.h"



/** Referes to the chat failure error cause */
typedef NS_ENUM(NSUInteger, ChatErrorCodes) {
    /** Failed to instantiate a successfull connection */
    CouldNotConnect = 1002,
    /** The current connection is broken */
    ChatConnectionFaild = 1003,
};


extern NSString * OperatorMessage;
extern NSString * CustomerMessage;
//extern const NSString * MessageFailToDeliver;
@class ChatManager;


/**
 Chat session delegate is responsible of handling the chat session callbacks
 */
@protocol ChatSessionDelegate <NSObject>

/**
 Called when a previously broken session connection has been re-established and the session re-joined successfully
 
 @param chatManager ChatManager instance
 */
-(void)chatManagerSessionDidRestore:(ChatManager *)chatManager;


/**
 Called when an attempt to start a new chat session is failed
 
 @param chatManager ChatManager instance
 @param errorMessage human-friendly message describes the potential cause of the error
 */
-(void)chatManager:(ChatManager *)chatManager didFailToEnterChat:(NSString *)errorMessage;




/**
 Called when a session is successfully started and a welcome message is received
 
 @param chatManager ChatManager instance
 @param welcomeMessage the human-friendly welcome message set for your server
 */
-(void)chatManager:(ChatManager *)chatManager didEnterChat:(NSString *)welcomeMessage;


/**
 Called when there is an update in the waiting queue
 
 @param chatManager ChatManager instance
 @param waitingData describes the current waiting status
 */
-(void)chatManager:(ChatManager *)chatManager waitingQueueDidUpdate:(ChatWaitingModel *)waitingData;

/**
 If the operator transfered the customer to another department and the customer entered a queue in the other department this method will be called
 
 @param chatManager ChatManager instance
 @param waitingData describes the current waiting status
 */
-(void)chatManager:(ChatManager *)chatManager didEnterWaitingQueueAfterTransfer:(ChatWaitingModel *)waitingData;

/**
 Called when the operator transfers the customer to another department
 
 @param chatManager ChatManager instance
 */
-(void)chatManagerSessionIsBeingTransfered:(ChatManager *)chatManager;



/**
 Called when the current established connection is broken for any reason
 
 @param chatManager ChatManager instance
 @param error check the "message" key in error.userInfo dictionary
 */
-(void)chatManager:(ChatManager *)chatManager connectionDidFail:(NSError *)error;

/**
 Called when the operator is disconnected
 
 @param chatManager ChatManager instance
 */
-(void)chatManagerOperatorDidDisconnect:(ChatManager *)chatManager;

/**
 Called when the operator is connected after he had a broken connection
 
 @param chatManager ChatManager instance
 */
-(void)chatManagerOperatorDidReconnect:(ChatManager *)chatManager;

/**
 Called when you enter a chat, and there are more than one department to select from
 When called, set the selectedDepartmentId value and then call enterChatWithCustomerName: customerId: customerLocation: deviceToken: method again
 
 @param chatManager ChatManager instance
 @param departments array of DepartmentModel to select one
 */
-(void)chatManager:(ChatManager *)chatManager aDepertmentMustBeSelectedFirst:(NSArray *)departments;

/**
 Called when an operator opens the chat session that the customer entered
 
 After the customer enters a chat, he should not send any message til this method is called.
 
 @param chatManager ChatManager instance
 @param message that can be displayed to the user to let him know that the operator is joined
 */
-(void)operatorDidJoin:(ChatManager *)chatManager joinMessage:(NSString *)message;


@optional
/**
 In very rare cases you will need to implement this method. It requires both: custom fields to be enabled for your server, and user management system is based on Bubbles user management system.
 
 If this method is called, it implicitly means the start of your chat session is faield.
 
 @param chatManager ChatManager instance
 @param fields array of custom fields to give the server permission to access
 
*/
 

-(void)chatManager:(ChatManager *)chatManager needsPermissionToAccessCustomFields:(NSArray *)fields;

/**
 In very rare cases you will need to implement this method. It requires both: custom fields to be enabled for your server, and user management system is based on Bubbles user management system.
 
 If this method is called, it implicitly means the start of your chat session is faield.
 
 @param chatManager ChatManager instance
 @param fields array of custom fields to give that the server requires the customer to fill in order to be able to chat
 
 */
-(void)chatManager:(ChatManager *)chatManager needsToCompleteCustomFields:(NSArray *)fields;

@end

/**
 Chat message delegate, is responsible of handling the various messages events
 */
@protocol ChatMessagesDelegate <NSObject>

/**
 Called when there are messages those sent by the user and failed to be delivered to the server
 
 @param chatManager ChatManager instance
 @param failedMessages array of ChatMessageModel that failed to be delivered
 */
-(void)chatManager:(ChatManager *)chatManager didFailToDeliverMessages:(NSArray *)failedMessages;


/**
 Called when the message is successfully sent to the server
 
 @param chatManager ChatManager instance
 @param message the successfully sent message
 */
-(void)chatManager:(ChatManager *)chatManager didSuccessToDeliverMessage:(ChatMessageModel *)message;


/**
 Called when a new message is received
 
 @param chatManager ChatManager instance
 @param message     ChatMessageModel the newly recieved message
 */
-(void)chatManager:(ChatManager *)chatManager didReceiveMessage:(ChatMessageModel *)message;


/**
 Called when the operator is typing on his keyboard
 
 @param chatManager ChatManager instance
 */
-(void)operatorIsTyping:(ChatManager *)chatManager;


/**
 Called when the operator is stopped typing
 
 @param chatManager ChatManager instance
 */
-(void)operatorStoppedTyping:(ChatManager *)chatManager;


/**
 Called when the operator leave the chat session. After the call of this method, no messages will be delivered.
 
 @param chatManager ChatManager instance
 @param chatEndMessage the bye bye message set in your server
 */
-(void)operatorDidLeftChat:(ChatManager *)chatManager chatEndMessaeg:(NSString *)chatEndMessage;


/**
 Called when the customer stop typing or sending messages for the a while.
 If the maximum allowed ideal time is 6 minutes, this method will be called if the user stop typing or sending messages for more than 3 minutes
 
 @param chatManager ChatManager instance
 @param idealNotificationMessage the ideal notification message set in the server configuration
 */
-(void)chatManagerUserIsIdeal:(ChatManager *)chatManager idealNotificationMessage:(NSString *)idealNotificationMessage;


/**
 Called when the customer stop typing or sending messages for the a while.
 If the maximum allowed ideal time is 6 minutes, this method will be called if the user stop typing or sending messages for more than 6 minutes
 
 @param chatManager ChatManager instance
 @param chatEndMessage the chat end message set in the server
 */
-(void)chatManagerUserDidTimeOut:(ChatManager *)chatManager chatEndMessag:(NSString *)chatEndMessage;


@optional

/**
 If your server supports custom fields. You must implement this method
 
 Ignore the implementation of these methods if your server does not support custom fields
 @param chatManager ChatManager instance
 @param fieldName the field name that the server needs to access it
 
 */
-(void)chatManager:(ChatManager *)chatManager operatorWantsToAccessFieldNamed:(NSString *)fieldName;

/**
 If your server supports custom fields. You must implement this method
 
 Ignore the implementation of these methods if your server does not support custom fields
 @param chatManager ChatManager instance
 @param fieldName the field name that the server needs its value
 
 */

-(void)chatManager:(ChatManager *)chatManager operatorRequestedValueForFieldNamed:(NSString *)fieldName;
@end


/**
 Chat Manager responsible of performing all the chat operations
 */
@interface ChatManager : NSObject



/**
 The selected department id
 */
@property (nonatomic, strong) NSString * selectedDepartmentId;

/**
 Session delegate
 */
@property (nonatomic, weak) id<ChatSessionDelegate> sessionDelegate;

/**
 Messages delegate
 */
@property (nonatomic, weak) id<ChatMessagesDelegate> chatMessagesDelegate;



/**
 Determines if the server is currently offline
 */
@property (nonatomic) BOOL serverIsOffline;



/**
 Determines if the current chat session is in a waiting queue
 */
@property (nonatomic) BOOL serverBusy;


/**
 Cache manager delegate
 */
@property (nonatomic, strong) id<ChatCacheManagerDelegate> cacheManager;


/**
 The current waiting queue properties
 */
@property (nonatomic, strong) ChatWaitingModel * waitingData;


/**
 Creates and returns a signltone instance of this class
 @return returns the shared singleton of the ChatManager
 */
+(instancetype)instance;

/**
 Opens new chat session and closes any previous ones.
 
 @param name of the client must be provided
 @param customerId optional customer id
 @param customerlocation optional current customer location
 @param deviceToken if you want this customer to be able to receive push notifications, please provide its device id

 */

-(void)enterChatWithCustomerName:(NSString *)name customerId:(NSString *)customerId customerLocation:(CustomerLocation *)customerlocation deviceToken:(NSString *)deviceToken;


/**
 Leaves the current chat session. Call it when the customer wants to end the current chat session
 */
-(void)customerLeftChat;


/**
 Notifies the operator that the customer is currently typing
 */
-(void)customerStartTyping;


/**
 Notifies the operator that the customer stopped typing
 */
-(void)customerStoppedTyping;


/**
 Sends the passed message to the server
 
 @param message to be sent
 */
-(void)sendChatMessage:(ChatMessageModel *)message;

/**
 Sends the passed comment as well as the passed rate for the current session
 
 @param comment the feedback comment
 @param rate the feedback rate
 */
-(void)sendFeedback:(NSString *)comment rate:(float)rate;

/**
 Gives the server access to the custom field of the specified name
 
 This method works only if you have an active chat session and custom fields are enabled for your server.
 @param fieldName the field name for which to give access.
 */
-(void)allowAccessToCustomField:(NSString *)fieldName;


/**
 Sets the value for the passed field
 
 This method works only if you have an active chat session and custom fields are enabled for your server.
 
 @param fieldName the field name to update its value.
 @param value the new value
 */
-(void)updateCustomField:(NSString *)fieldName withValue:(NSString *)value;

/**
 Once you enter a chat, this method will return true till you call cleanUp
 */
-(BOOL)hasActiveSession;


/**
 Disconnect the current connection if any, cleans up any session-related data.
 After the chat session is finished (because the customer ends it, or you will no longer send or receive any messages), you must call this method to do the final clean up. If you will submit a feedback, call this method after submitting the feedback otherwise, call it once you no longer want the current chat session.
 */
-(void)cleanUp;

@end
