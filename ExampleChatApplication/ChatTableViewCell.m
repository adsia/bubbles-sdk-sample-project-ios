//
//  ChatCellTableViewCell.m
//  ExampleChatApplication
//
//  Created by Ahmed Ali on 1/27/15.
//  Copyright (c) 2015 Ahmed Ali. All rights reserved.
//

#import "ChatTableViewCell.h"
#import "UIImage+Utils.h"
#import "UIImageView+WebCache.h"


@interface ChatTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *sendingIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bubbleView;



@end

@implementation ChatTableViewCell

@synthesize message = _message;

-(void)setMessage:(ChatMessageModel *)message
{
    _message = message;
    [self refreshUI];
}


-(void)refreshUI
{
    self.messageLabel.text = self.message.msg;
    self.messageLabel.preferredMaxLayoutWidth = 180;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:self.message.timestamp];
    self.timeLabel.text = [formatter stringFromDate:date];
    
    [self.sendingIndicator stopAnimating];
    self.sendingIndicator.hidden = YES;
    self.statusImageView.hidden = YES;
    
    
    
    UIImageOrientation bubbleOrientation = UIImageOrientationUp;
    
    
    UIColor * color = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
    self.messageLabel.textColor = [UIColor lightGrayColor];
   
    
    if ([self.message.who isEqualToString:OperatorMessage])
    {
        bubbleOrientation =UIImageOrientationUpMirrored;
    
        color = [UIColor colorWithRed:0.6 green:0.6 blue:0.4 alpha:1.0];
        self.messageLabel.textColor = [UIColor darkGrayColor];
        
    }
    
    
    UIImage *coloredImage = [[UIImage imageNamed:@"ImageBubbleMask"] maskWithColor:color];
    UIImage *backgroundImageNormal = [[UIImage imageWithCGImage:coloredImage.CGImage
                                                          scale:1.0 orientation: bubbleOrientation] resizableImageWithCapInsets:UIEdgeInsetsMake(13, 13, 13, 21) resizingMode:UIImageResizingModeStretch];
    
    self.bubbleView.image = backgroundImageNormal;
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:self.message.avatar] placeholderImage:[UIImage imageNamed:@"flower"]];
    if([self.message.who isEqualToString:OperatorMessage]){
        return;
    }
    
    switch (self.message.status) {
        case MESSAGE_DELIVERED:{
            self.statusImageView.hidden = NO;
            self.statusImageView.image = [UIImage imageNamed:@"delivered"];
            break;
        }
        case MESSAGE_FAILED:{
            self.statusImageView.hidden = NO;
            self.statusImageView.image = [UIImage imageNamed:@"failed"];
            break;
        }
        case MESSAGE_SENT:{
            self.sendingIndicator.hidden = NO;
            [self.sendingIndicator startAnimating];
            break;
        }
            
        default:
            break;
    }
    
}

-(CGFloat)neededHeight
{
    return [self bubbleHeight] + 45;
}

-(CGFloat)bubbleHeight
{
    CGRect textRect = [self.message.msg boundingRectWithSize:CGSizeMake(180, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.messageLabel.font} context:nil];
    
    CGFloat targetHeight = textRect.size.height + 30;//top and bottom margins
    return MAX(76, targetHeight);
}


@end

