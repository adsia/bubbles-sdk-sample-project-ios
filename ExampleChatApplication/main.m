//
//  main.m
//  ExampleChatApplication
//
//  Created by Ahmed Ali on 1/22/15.
//  Copyright (c) 2015 Ahmed Ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
