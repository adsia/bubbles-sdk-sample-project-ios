//
//  ViewController.m
//  ExampleChatApplication
//
//  Created by Ahmed Ali on 1/22/15.
//  Copyright (c) 2015 Ahmed Ali. All rights reserved.
//

#import <BubblesSDK/BubblesSDK.h>
#import "ViewController.h"

#import "ChatViewController.h"
#import "TSMessage.h"


@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameField;

@end

@implementation ViewController


- (IBAction)startChat:(id)sender
{
    NSString * name = self.nameField.text;
    if(name.length > 0){
        [self performSegueWithIdentifier:@"showChat" sender:self];
    }else{
        [TSMessage showNotificationWithTitle:@"Opps!" subtitle:@"Please enter your name first" type:TSMessageNotificationTypeError];
    }
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
#warning Replace the data with yours
    [ConfigManager setServerName:<#ServerName#> APIKey:<#APIKey#>];
    ChatViewController * chatVc = segue.destinationViewController;
    chatVc.customerName = self.nameField.text;
}

@end
