//
//  UIImage.h
//  ExampleChatApplication
//
//  Created by Ahmed Ali on 1/27/15.
//  Copyright (c) 2015 Ahmed Ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Utils)
- (UIImage *) maskWithColor:(UIColor *) color;
@end
