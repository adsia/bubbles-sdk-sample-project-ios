//
//  ChatCellTableViewCell.h
//  ExampleChatApplication
//
//  Created by Ahmed Ali on 1/27/15.
//  Copyright (c) 2015 Ahmed Ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BubblesSDK/BubblesSDK.h>

@interface ChatTableViewCell : UITableViewCell

@property (nonatomic) ChatMessageModel * message;

-(void)refreshUI;
-(CGFloat)neededHeight;
-(CGFloat)bubbleHeight;
@end
