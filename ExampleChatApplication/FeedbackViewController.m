//
//  FeedbackViewController.m
//  ExampleChatApplication
//
//  Created by Ahmed Ali on 1/27/15.
//  Copyright (c) 2015 Ahmed Ali. All rights reserved.
//

#import <BubblesSDK/BubblesSDK.h>
#import "FeedbackViewController.h"
#import "EDStarRating.h"
#import "TSMessage.h"

@interface FeedbackViewController ()
@property (weak, nonatomic) IBOutlet UITextView *feedbackTextView;
@property (weak, nonatomic) IBOutlet EDStarRating *starRating;

@end

@implementation FeedbackViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.starRating.editable = YES;
    self.starRating.rating = 3;
    self.starRating.maxRating = 5;
    self.starRating.starImage = [UIImage imageNamed:@"starOff"];
    self.starRating.starHighlightedImage = [UIImage imageNamed:@"starOn"];
    self.starRating.horizontalMargin = 16;
    
}

- (IBAction)cancel:(id)sender
{
    [[ChatManager instance] cleanUp];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)send:(id)sender
{
    NSString * message = self.feedbackTextView.text;
    if(message.length == 0){
        [TSMessage showNotificationWithTitle:@"Opps!" subtitle:@"Please enter your feedback comment" type:TSMessageNotificationTypeError];
        return;
    }
    CGFloat rate = self.starRating.rating;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [[ChatManager instance] sendFeedback:message rate:rate];
        [[ChatManager instance] cleanUp];
    });
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
